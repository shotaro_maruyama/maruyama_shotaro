package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;

@WebServlet("/deleteComment")
public class DeleteCommentServlet extends HttpServlet {
	 //URLパターンを打ち込まれても実行しないようPOSTで実装
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
		int comment_Id = Integer.parseInt(request.getParameter("comment_id"));

		new CommentService().delete(comment_Id);
		response.sendRedirect("./");
	}

}

