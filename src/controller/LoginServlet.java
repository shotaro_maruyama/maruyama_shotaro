package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws IOException, ServletException {

			request.getRequestDispatcher("login.jsp").forward(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		User user = new UserService().select(account, password);
		List<String> errorMessages = new ArrayList<String>();

		//userにnullが格納されている時点でDBにデータが無い
		if (user == null) {
			boolean certainError;
			certainError= false;

			if(account.isEmpty()) {
				errorMessages.add("アカウント名を入力してください");
				certainError = true;
			}
			if(password.isEmpty()) {
				errorMessages.add("パスワードを入力してください");
				certainError = true;
			}
			if(! certainError) {
				errorMessages.add("アカウントもしくはパスワードが間違っています。");
			}

			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;

		}else if (user.getIs_Stopped() == 1) {
			errorMessages.add("アカウントもしくはパスワードが間違っています");
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}
}
