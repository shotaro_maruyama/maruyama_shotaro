package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {

		int isStopNum = Integer.valueOf(request.getParameter("is_stopped"));
		int id = Integer.valueOf(request.getParameter("user_id"));

		new UserService().switchStatus(id,isStopNum);
		response.sendRedirect("management");
	}
}