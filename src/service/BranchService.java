package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;

public class BranchService{
	/**
	 * 全支社を取得
	 * @return
	 */
	public List<Branch> select() {
		Connection connection = null;
		try {
			connection = getConnection();
			List<Branch> allBranch = new BranchDao().select(connection);
			commit(connection);

			return allBranch;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
