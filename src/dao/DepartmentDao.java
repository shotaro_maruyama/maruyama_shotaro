package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {
	//ユーザー一覧取得用セレクトメソッド
	public List<Department> select(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments ORDER BY id DESC ";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Department> allDepartment = toAllDepartment(rs);
			return allDepartment;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toAllDepartment(ResultSet rs)
				throws SQLException {

		List<Department> allDepartment = new ArrayList<Department>();
		try {
			while (rs.next()) {
				Department department = new Department();
				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreated_Date(rs.getTimestamp("created_date"));
				department.setUpdated_Date(rs.getTimestamp("updated_date"));

				allDepartment.add(department);
			}
			return allDepartment;
		} finally {
				close(rs);
		}
	}
}