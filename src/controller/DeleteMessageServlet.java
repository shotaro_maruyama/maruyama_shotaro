package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.MessageService;

@WebServlet("/deleteMessage")
public class DeleteMessageServlet extends HttpServlet {
	 //URLパターンを打ち込まれても実行しないようPOSTで実装
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
		int message_Id = Integer.parseInt(request.getParameter("message_id"));

		new MessageService().delete(message_Id);
		response.sendRedirect("./");
	}
}

