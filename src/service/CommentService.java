package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;

public class CommentService {

		/**
		 * 新規コメントを提供
		 * @param comment
		 */
		public void insert(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	/**
	 * コメント一覧取得を提供
	 * @return
	 */
	public List<UserComment> select() {

		Connection connection = null;
		try {
			connection = getConnection();
			List<UserComment> comments = new UserCommentDao().select(connection);
			commit(connection);

			return comments;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	/**
	 * コメント削除を提供
	 * @param comment_Id
	 */
	public void delete(int comment_Id) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().delete(connection, comment_Id);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}