package  filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns= {"/*"
										})
public class LoginFilter implements Filter{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		String path = ((HttpServletRequest) request).getServletPath();
		List<String> errorMessages = new ArrayList<String>();
		System.out.println(path);


		if(!(path.equals("/login")) && (!(path.matches(".*css")))) {
			if (session.getAttribute("loginUser") == null) {
				errorMessages.add("ログインしてください");
				session.setAttribute("errorMessages", errorMessages);
				((HttpServletResponse) response).sendRedirect("./login");
				return;
			}else if(session.getAttribute("loginUser") != null) {
				chain.doFilter(request, response);
				return;
			}
		}else{
			chain.doFilter(request, response);
			return;
		}
	}

	public void init(FilterConfig config) {
	}
	public void destroy() {
	}
}