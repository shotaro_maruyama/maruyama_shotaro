package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

//このUserCommentDaoは表結合して参照しているだけのクラス
public class UserCommentDao {
	public List<UserComment> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("		users.id AS user_id, ");
			sql.append("		users.name AS name, ");
			sql.append("		comments.id AS comment_id, ");
			sql.append("		comments.message_id AS message_id, ");
			sql.append("		comments.text AS text, ");
			sql.append("		comments.created_date AS created_date ");
			sql.append("FROM users ");
			sql.append("INNER JOIN comments ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC " );//DESC＝>降順の指定最新から表示　limit=>＜[表示開始添字]＞,＜表示行＞

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> comments = toUserComments(rs);
			return comments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserComments(ResultSet rs)
				throws SQLException {

			List<UserComment> comments = new ArrayList<UserComment>();
			try {
				while (rs.next()) {
					UserComment comment = new UserComment();
					comment.setUser_Id(rs.getInt("user_id"));
					comment.setId(rs.getInt("comment_id"));
					comment.setUser_Name(rs.getString("name"));
					comment.setText(rs.getString("text"));
					comment.setMessage_Id(rs.getInt("message_id"));
					comment.setCreated_Date(rs.getTimestamp("created_date"));

					comments.add(comment);
				}
				return comments;
			} finally {
				close(rs);
			}
		}
}