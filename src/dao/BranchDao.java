package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {
	//全社名取得用セレクトメソッド
	public List<Branch> select(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches ORDER BY id ASC ";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Branch> allBranch = toAllBranch(rs);
			return allBranch;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toAllBranch(ResultSet rs)
				throws SQLException {

		List<Branch> allBranch = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				Branch branch = new Branch();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branch.setCreated_Date(rs.getTimestamp("created_date"));
				branch.setUpdated_Date(rs.getTimestamp("updated_date"));

				allBranch.add(branch);
			}
			return allBranch;
		} finally {
				close(rs);
		}
	}
}