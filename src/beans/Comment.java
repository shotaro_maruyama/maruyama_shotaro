package beans;

import java.util.Date;

public class Comment {

	private int id; //コメント自体のID
	private String text;
	private int user_Id; //投稿したユーザーのID
	private int message_Id; //コメント先のメッセージID
	private Date created_Date;
	private Date updated_Date;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(int user_Id) {
		this.user_Id = user_Id;
	}
	public int getMessage_Id() {
		return message_Id;
	}
	public void setMessage_Id(int message_Id) {
		this.message_Id = message_Id;
	}
	public Date getCreated_Date() {
		return created_Date;
	}
	public void setCreated_Date(Date created_Date) {
		this.created_Date = created_Date;
	}
	public Date getUpdated_Date() {
		return updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		this.updated_Date = updated_Date;
	}
}