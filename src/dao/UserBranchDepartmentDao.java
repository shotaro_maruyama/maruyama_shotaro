package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {
	//ユーザー一覧取得用セレクトメソッド
	public List<UserBranchDepartment> select(Connection connection) {
		PreparedStatement ps = null;
		try {
			String num = "100" + " ";
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("		users.id AS user_id, ");
			sql.append("		users.account AS user_account, ");
			sql.append("		users.name AS user_name, ");
			sql.append("		users.is_stopped AS user_isstopped, ");
			sql.append("		users.created_date AS created_date, ");
			sql.append("		users.updated_date AS updated_date, ");
			sql.append("		branches.id AS branch_id, ");
			sql.append("		branches.name AS branch_name, ");
			sql.append("		departments.id AS department_id, ");
			sql.append("		departments.name AS department_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY users.created_date DESC limit " + num );//DESC＝>降順の指定最新から表示　limit=>＜[表示開始添字]＞,＜表示行＞

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> users = toAllUser(rs);

			return users;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toAllUser(ResultSet rs)
			throws SQLException {

	List<UserBranchDepartment> allUser = new ArrayList<UserBranchDepartment>();
	try {
		while (rs.next()) {
			UserBranchDepartment user = new UserBranchDepartment();
			user.setId(rs.getInt("user_id"));
			user.setAccount(rs.getString("user_account"));
			user.setName(rs.getString("user_name"));
			user.setBranch_Id(rs.getInt("branch_id"));
			user.setBranch_Name(rs.getString("branch_name"));
			user.setDepartment_Id(rs.getInt("department_id"));
			user.setDepartment_Name(rs.getString("department_name"));
			user.setCreated_Date(rs.getTimestamp("created_date"));
			user.setUpdated_Date(rs.getTimestamp("updated_date"));
			user.setIs_Stopped(rs.getInt("user_isstopped"));
			allUser.add(user);
		}
		return allUser;
	} finally {
			close(rs);
	}
}
	//ユーザー編集用セレクトメソッド
	public UserBranchDepartment select(Connection connection, int user_Id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("		users.id AS user_id, ");
			sql.append("		users.account AS user_account, ");
			sql.append("		users.name AS user_name, ");
			sql.append("		users.is_stopped AS user_isstopped, ");
			sql.append("		users.created_date AS created_date, ");
			sql.append("		users.updated_date AS updated_date, ");
			sql.append("		branches.id AS branch_id, ");
			sql.append("		branches.name AS branch_name, ");
			sql.append("		departments.id AS department_id, ");
			sql.append("		departments.name AS department_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("WHERE users.id = " + user_Id);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			UserBranchDepartment user = toUser(rs);
			if(user.getAccount() == null) {
				return null;
			}

			return user;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private UserBranchDepartment toUser(ResultSet rs)
			throws SQLException {

		UserBranchDepartment user = new UserBranchDepartment();
		try {
			while (rs.next()) {
				user.setId(rs.getInt("user_id"));
				user.setAccount(rs.getString("user_account"));
				user.setName(rs.getString("user_name"));
				user.setBranch_Id(rs.getInt("branch_id"));
				user.setBranch_Name(rs.getString("branch_name"));
				user.setDepartment_Id(rs.getInt("department_id"));
				user.setDepartment_Name(rs.getString("department_name"));
				user.setCreated_Date(rs.getTimestamp("created_date"));
				user.setUpdated_Date(rs.getTimestamp("updated_date"));
				user.setIs_Stopped(rs.getInt("user_isstopped"));
			}
			return user;
		} finally {
				close(rs);
		}
	}
}