package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DepartmentDao;

public class DepartmentService{
	/**
	 * 全部署を取得
	 * @return
	 */
	public List<Department> select() {
		Connection connection = null;
		try {
			connection = getConnection();
			List<Department> allDepartment = new DepartmentDao().select(connection);
			commit(connection);

			return allDepartment;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
