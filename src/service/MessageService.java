package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {
	/**
	 * 新規投稿を提供
	 * @param message
	 */
	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
		/**
		 * ユーザーと投稿のDB取得を提供
		 * @param start
		 * @param end
		 * @param category
		 * @return
		 */
	public List<UserMessage> select(String start, String end, String category) {

		Connection connection = null;
		try {
			connection = getConnection();
			String stringStart = null;
			String stringEnd = null;
			SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(StringUtils.isEmpty(start)) {
				//デフォルト値
				 stringStart = "2020-01-01 00:00:00";
			}else if(!(StringUtils.isEmpty(start))) {
				stringStart = start + " 00:00:00";
			}

			if(StringUtils.isEmpty(end)) {
				//デフォルト値現在時刻
				Date date = new Date();
				stringEnd =formatDate.format(date); //文字列型の現在時刻
			}else if(!(StringUtils.isEmpty(end))) {
				stringEnd = end +" 23:59:59";
			}

			if(!(StringUtils.isEmpty(category))) {
				category = "%" + category +"%";
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, stringStart, stringEnd, category);
			commit(connection);

			return messages;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 *投稿削除を提供
	 * @param message_Id
	 */
	public void delete(int message_Id) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, message_Id);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//***********************************************
}