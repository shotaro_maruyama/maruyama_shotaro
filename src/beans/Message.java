package beans;

import java.util.Date;

public class Message {

	private int id; //投稿内容のID
	private String title;
	private String category;
	private String text;
	private int user_Id; //投稿したユーザーのID
	private Date created_Date;
	private Date updated_Date;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(int user_Id) {
		this.user_Id = user_Id;
	}
	public Date getCreated_Date() {
		return created_Date;
	}
	public void setCreated_Date(Date created_Date) {
		this.created_Date = created_Date;
	}

	//不要であれば削除
	public Date getUpdated_Date() {
		return updated_Date;
	}
	//不要であれば削除
	public void setUpdated_Date(Date updated_Date) {
		this.updated_Date = updated_Date;
	}
}