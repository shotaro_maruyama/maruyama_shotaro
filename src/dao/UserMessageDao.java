package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

//このUserMessageDaoは表結合して参照しているだけのクラス
public class UserMessageDao {
	public List<UserMessage> select(Connection connection, String stringStart, String stringEnd, String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("		users.name as name, ");
			sql.append("		users.id as user_id, ");
			sql.append("		messages.id as id, ");
			sql.append("		messages.title as title, ");
			sql.append("		messages.category as category, ");
			sql.append("		messages.text as text, ");
			sql.append("		messages.created_date as created_date ");
			sql.append("FROM users ");
			sql.append("INNER JOIN messages ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date BETWEEN " );
			sql.append("    ? "); //年月日
			sql.append("    AND ");
			sql.append("    ? "); //年月日
			if(!StringUtils.isEmpty(category)) {
				sql.append("AND category LIKE ");
				sql.append("    ? ");
			}
			sql.append("ORDER BY messages.created_date DESC");//DESC＝>降順の指定最新から表示　limit=>＜[表示開始添字]＞,＜表示行＞


			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, stringStart);
			ps.setString(2, stringEnd);
			if(!StringUtils.isEmpty(category)) {
				ps.setString(3, category);
			}
			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);
			return messages;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs)
				throws SQLException {

			List<UserMessage> messages = new ArrayList<UserMessage>();
			try {
				while (rs.next()) {
					UserMessage message = new UserMessage();
					message.setId(rs.getInt("id"));
					message.setUser_Id(rs.getInt("user_id"));
					message.setName(rs.getString("name"));
					message.setTitle(rs.getString("title"));
					message.setCategory(rs.getString("category"));
					message.setText(rs.getString("text"));
					message.setCreated_Date(rs.getTimestamp("created_date"));

					messages.add(message);
				}
				return messages;
			} finally {
					close(rs);
			}
		}
}