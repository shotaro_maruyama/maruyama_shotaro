package  filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns= {	"/management","/setting",
										"/signup","/stop"
										})
public class ManagementFilter implements Filter{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		if (session.getAttribute("loginUser") == null) {
			((HttpServletResponse) response).sendRedirect("./login");
			return;
		}

		User user = (User) session.getAttribute("loginUser");

		if (user.getDepartment_Id() != 1) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("アクセス権限がありません"); //session領域に格納するとremoveする場所は？
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("./");
			return;
		}else {
			chain.doFilter(request, response);
			return;
		}
	}

	public void init(FilterConfig config) {
	}
	public void destroy() {
	}
}
