package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		String commentConts = request.getParameter("comment");

		if (!isValid(commentConts,errorMessages)) {
				session.setAttribute("errorMessages", errorMessages);
				response.sendRedirect("./");
				return;
		}

		Comment comment = new Comment();

		comment.setText(request.getParameter("comment"));
		User user = (User) session.getAttribute("loginUser");
		comment.setMessage_Id(Integer.parseInt(request.getParameter("message_id")));
		comment.setUser_Id(user.getId());

		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private boolean isValid(String commentConts, List<String> errorMessages) {

		if ((StringUtils.isEmpty(commentConts)) ||((commentConts.matches("[　]+")))
			|| (commentConts.matches("[\\s]+"))) {
				errorMessages.add("未入力です");
		}

		if (commentConts.length() > 500) {
			errorMessages.add("コメントは５００文字以内で入力してください");
		}

		if (errorMessages.size() != 0) {
				return false;
		}

		return true;
	}
}
