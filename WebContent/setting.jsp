<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <link rel="stylesheet" type="text/css" href="./css/default.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー編集</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

			<a href="management">ユーザー管理画面</a>

            <form action="setting" method="post">

                <label for="account">アカウント名</label>
                <input type = "text" name="account" id="account" value ="${user.account}"/> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

				<label for="passwordCheck">確認用パスワード</label>
                <input name="passwordCheck" type="password" id="passwordCheck" /> <br />

                <label for="name">名前</label>
                <input type = "text" name="name" id="name" value ="${user.name}"/> <br />

				<label for="branch_id">支社</label>
                <select name="branch_id" id="branch_id">
					<c:forEach items="${allBranch}" var="branch">
						<c:if test = "${branch.id == user.branch_Id }">
							<option value= "${branch.id}" selected>${branch.name}</option>
						</c:if>
						<c:if test = "${loginUser.id != user.id }">
							<c:if test = "${branch.id != user.branch_Id }">
								<option value= "${branch.id}">${branch.name}</option>
							</c:if>
						</c:if>
					</c:forEach>
				</select>

				<label for="department_id">部署</label>
				<select name="department_id" id="department_id">
					<c:forEach items="${allDepartment}" var="department">

						<c:if test = "${department.id == user.department_Id }">
							<option value= "${department.id}" selected>${department.name}</option>
						</c:if>
						<c:if test = "${loginUser.id != user.id }">
							<c:if test = "${department.id != user.department_Id }">
								<option value= "${department.id}" >${department.name}</option>
							</c:if>
						</c:if>
					</c:forEach>
				</select>


			 <input type="hidden" name="user_id" value="${user.id}">
             <input type="submit" value="更新" /> <br />
            </form>

        </div>
    </body>
</html>