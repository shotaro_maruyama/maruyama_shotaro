package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		if (!isValid(title,text,category,errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("title", title);
			request.setAttribute("category", category);
			request.setAttribute("text", text);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		Message message = new Message();
		message.setTitle(title);
		message.setCategory(category);
		message.setText(text);

		User user = (User) session.getAttribute("loginUser");//セッション領域に格納したユーザー情報をBeanに格納
		message.setUser_Id(user.getId());

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	private boolean isValid(String title, String text,  String category, List<String> errorMessages) {

		if ((StringUtils.isEmpty(title)) || title.matches("^[\\s|　]+$")) {
			errorMessages.add("タイトルを入力してください");
		}else if(30 < title.length()) {
			errorMessages.add("タイトルは３０文字以下で入力してください");
		}

		if((StringUtils.isEmpty(category)) || category.matches("^[\\s|　]+$")) {
			errorMessages.add("カテゴリを入力してください");
		}else if (10 < category.length()) {
			errorMessages.add("カテゴリは１０文字以下で入力してください");
		}

		if((StringUtils.isEmpty(text)) || text.matches("^[\\s|　]+$")) {
			errorMessages.add("本文を入力してください");
		}else if (1000 < text.length()) {
			errorMessages.add("本文は1000文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
				return false;
		}
		return true;
	}
}
