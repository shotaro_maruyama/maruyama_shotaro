package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import beans.UserBranchDepartment;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {

		UserBranchDepartment user = new UserBranchDepartment();
		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest) request).getSession();
		String userId = request.getParameter("user_id");

		if(userId.matches("^[0-9]+$")){

			user = new UserService().select(Integer.parseInt(request.getParameter("user_id")));
			if(user == null){
				errorMessages.add("不正な情報が入力されました");
				session.setAttribute("errorMessages", errorMessages);
				response.sendRedirect("management");
				return;
			}
		}else {
			errorMessages.add("不正な情報が入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("management");
			return;
		}

		List<Branch> allBranch = new BranchService().select();
		List<Department> allDepartment = new DepartmentService().select();

		request.setAttribute("allBranch", allBranch);
		request.setAttribute("allDepartment", allDepartment);
		request.setAttribute("user", user);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
		List<Branch> allBranch = new BranchService().select();
		List<Department> allDepartment = new DepartmentService().select();
		List<String> errorMessages = new ArrayList<String>();
		User user = getUser(request);

		if (!isValid(user, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("allBranch", allBranch);
			request.setAttribute("allDepartment", allDepartment);
			request.setAttribute("user", user); //management画面からuser情報を受け取るためRedirectが出来ない
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		new UserService().update(user);
		response.sendRedirect("management");
	}

	//DB操作ではないのでDAOパターンには任せない
	private User getUser(HttpServletRequest request)
				throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.valueOf(request.getParameter("user_id")));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setPasswordCheck(request.getParameter("passwordCheck"));
		user.setName(request.getParameter("name"));
		user.setBranch_Id(Integer.valueOf(request.getParameter("branch_id")));
		user.setDepartment_Id(Integer.valueOf(request.getParameter("department_id")));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

		int id = user.getId();
		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String passwordCheck = user.getPasswordCheck();
		int branch_Id = user.getBranch_Id();
		int department_Id = user.getDepartment_Id();
		List<UserBranchDepartment> allUser = new UserService().select();

		for(int i = 0; i < allUser.size(); i++) {
			if( (account.equals(allUser.get(i).getAccount()) && (id != allUser.get(i).getId()) )) {
				errorMessages.add("アカウント名が重複しています");
			}
		}

		if((StringUtils.isEmpty(account)) || account.matches("^[\\s|　]+$")) {
			errorMessages.add("アカウント名を入力してください");
		}else if(20 < account.length() || 6 > account.length()) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		}

		if(	(!(account.matches("^[a-zA-Z0-9]+$")))){
			errorMessages.add("アカウントに使用できるのは半角英数字のみです");
		}

		//排除したい異常系は「どちらか片方だけ空欄」の場合なので排他的論理和を使用
		if(((StringUtils.isEmpty(password)) ^(StringUtils.isEmpty(passwordCheck))) ) {
			errorMessages.add("パスワードを入力してください");
		}

		if(((!StringUtils.isEmpty(password)) && (!StringUtils.isEmpty(passwordCheck))) ) {
			if(	(!(password.matches("^[a-zA-Z0-9!-/:-@\\[-`{-~]+$")))){
				errorMessages.add("パスワードに使用できるのは半角英数字記号のみです");
			}

			if(! passwordCheck.equals(password)) {
				errorMessages.add("確認用パスワードと一致しません");
			}

			if(20 < password.length() || 6 > password.length()) {
				errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
			}
		}

		if(StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		} else {
			if(10 < name.length()){
				errorMessages.add("名前は10文字以下で入力してください");
			}
			if(	(name.matches("^(?=.*[\\s]).*$")) || (name.matches("^(?=.*[　]).*$"))){
				errorMessages.add("名前に空白は使用できません");
			}
		}

		if((branch_Id == 1) && (department_Id >= 3)) {
			errorMessages.add("支社と部署の組み合わせが正しくありません");
		}else if((branch_Id >= 2) && (department_Id <= 2)) {
			errorMessages.add("支社と部署の組み合わせが正しくありません");
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}