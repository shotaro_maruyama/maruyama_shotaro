package beans;

import java.util.Date;

public class Branch {
	private int id;
	private String name;
	private Date created_Date;
	private Date updated_Date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreated_Date() {
		return created_Date;
	}
	public void setCreated_Date(Date created_Date) {
		this.created_Date = created_Date;
	}
	public Date getUpdated_Date() {
		return updated_Date;
	}
	public void setUpdated_Date(Date updated_Date) {
		this.updated_Date = updated_Date;
	}

}