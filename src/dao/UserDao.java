package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {
	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			int stopNum = 0;
			sql.append("INSERT INTO users ( ");
			sql.append("    account, ");
			sql.append("    password, ");
			sql.append("    name, ");
			sql.append("    branch_id, ");
			sql.append("    department_id, ");
			sql.append("    is_stopped, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");                                  // 1account
			sql.append("    ?, ");                                  // 2password
			sql.append("    ?, ");                                  // 3name
			sql.append("    ?, ");                                  // 4branch
			sql.append("    ?, ");                                  // 5division
			sql.append("    " + stopNum + ", ");           // is_stopped 非停止を表す0　
			sql.append("    CURRENT_TIMESTAMP, ");  // created_date
			sql.append("    CURRENT_TIMESTAMP ");  // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch_Id());
			ps.setInt(5, user.getDepartment_Id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }

	//ログイン用セレクトメソッド
	public User select(Connection connection, String account, String password) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs); //ログインした時点で↓のtoUsersリストにDBからのUser情報が格納されている

			if (users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET ");

			sql.append("		account = ?, ");
			sql.append("		name = ?, ");
			sql.append("		branch_id = ?, ");
			sql.append("		department_id = ?, ");

			if(! StringUtils.isEmpty(user.getPassword())) {
				sql.append("		password = ?, ");
			}
			sql.append("		updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch_Id());
			ps.setInt(4, user.getDepartment_Id());
			if(! StringUtils.isEmpty(user.getPassword())) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			}else {
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();

			if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
				throw new SQLRuntimeException(e);
		} finally {
				close(ps);
		}
	}

	public void switchStatus(Connection connection, int id, int isStopNum) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("		is_stopped = ?, ");
			sql.append("		updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, isStopNum);
			ps.setInt(2, id );

			int count = ps.executeUpdate();
			if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
				throw new SQLRuntimeException(e);
		} finally {
				close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs) throws SQLException {
		List<User> users = new ArrayList<User>();
		try {
			while (rs.next()) {
				User user = new User();
				user.setId(Integer.parseInt(rs.getString("id")));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranch_Id(Integer.parseInt(rs.getString("branch_id")));
				user.setDepartment_Id(Integer.parseInt(rs.getString("department_id")));
				user.setIs_Stopped(Integer.parseInt(rs.getString("is_stopped")));
				user.setCreated_Date(rs.getTimestamp("created_date"));
				user.setUpdated_Date(rs.getTimestamp("updated_date"));

				users.add(user);
			}
			return users; //リストにUserクラス型を入れて返す
		} finally {
				close(rs);
		}
	}
}