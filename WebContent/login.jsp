<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
     	<link rel="stylesheet" type="text/css" href="./css/login.css">
		<link rel="stylesheet" type="text/css" href="./css/default.css">
     	<%--
     	 --%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul class = "errorMessages">
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <c:remove var = "errorMessages" scope = "session"></c:remove>

            <form action="login" method="post"><br />
                <label for="account">アカウント</label>
                <input name="account" id="account" value ="${account}"/> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />


                <div class = "loginButton"><input type="submit" value="ログイン" /><br /></div>
            </form>

            <div class="copyright"> Copyright(c)ShotaroMaruyama</div>
        </div>
    </body>
</html>