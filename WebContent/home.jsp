<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <link rel="stylesheet" type="text/css" href="./css/home.css">
    <link rel="stylesheet" type="text/css" href="./css/default.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
</head>
    <body>
        <div class="main-contents">

			<a href = "message">新規投稿</a>
				<c:if test="${loginUser.department_Id == 1}" >
					<a href = "management">ユーザー管理</a>
				</c:if>
			<a href = "logout">ログアウト</a>

			<script type="text/javascript">
			function check(){

				if(window.confirm('削除してよろしいですか')){ // 確認ダイアログを表示
					return true; // 「OK」時は送信を実行
				}
				else{ // 「キャンセル」時の処理
					window.alert('キャンセルされました'); // 警告ダイアログを表示
					return false; // 送信を中止
				}
			}
			</script>

			<p>
			<c:if test="${ not empty loginUser }">
			    <div class="profile">
			       ログイン中: <c:out value="${loginUser.name}" />
			    </div>
			</c:if>
			</p>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                     <ul class = "errorMessages">
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

			<c:remove var = "errorMessages" scope = "session"></c:remove>

			 <form action="index.jsp" method="get">
			 		日付<input type="date" name="start" id="start" value = "${start}"/>
					～<input type="date" name="end" id="end" value = "${end}"/> <br />
					カテゴリ<input  name = "category" id = "category" value = "${category}">
					<input type="submit" value="絞り込み	">
			 </form>

			<h1>みんなの投稿 </h1>

			<div class="messages">
			    <c:forEach items="${messages}" var="message">
			        <div class="message">
				        <div class = "messageHeader">
			                <div class ="contributer"><c:out value="@${message.name}" /></div>
			                <c:if test="${ user.id == message.user_Id }">
							    <form action="deleteMessage" method="post" onSubmit="return check()">
								    <input type="hidden" name="message_id" value="${message.id}">
								  	<div class = "deletePostButton"><input  type="submit" value="投稿削除"></div>
							    </form>
							</c:if>
						</div>

						<div class = "messageBody">
			                <c:out value="${message.title}" />
				            <fmt:formatDate value="${message.created_Date}" pattern="yyyy/MM/dd HH:mm:ss" /><br />
			                <div class = "category"><c:out value="#${message.category}" /></div>

							<c:forEach var="splitedText" items="${fn:split(message.text, '
')}">
							    <div class = "text">${splitedText}</div>
							</c:forEach>
						</div>

						<c:forEach items="${comments}" var="comment">
			            	<c:if test="${ comment.message_Id == message.id }">
		            			<div class="comments">
					            	<c:out value="@${comment.user_Name}" />
					            	<div class = "reply"><c:out value=" To${message.name}" /></div>

				            		<div class = "comment">
										<c:forEach var="splitedComment" items="${fn:split(comment.text, '
')}">
										    <div>${splitedComment}</div>
										</c:forEach>

						            	<c:out value="${comment.created_Date}" /><br />

										<c:if test="${ user.id == comment.user_Id }">
								          	 <form action="deleteComment" method="post" onSubmit="return check()">
											    <input type="hidden" name="comment_id" value="${comment.id}">
											    <div class="deleteCommentButton"><input type="submit" value="コメント削除"></div>
										    </form>
									    </c:if>
									</div>
			          		 	</div>
				            </c:if>
		                </c:forEach>

			            <form action="comment" method="post">
							<label for="comment">コメント入力欄</label><br />
							<textarea name="comment" cols="30" rows="3" class="textbox"></textarea><br />
							<input type="hidden" name="message_id" value="${message.id}">
							<div class="postCommentButton"><input type="submit" value="コメント投稿"></div>
						</form>
			        </div>
			    </c:forEach>
			</div>


            <div class="copyright"> Copyright(c)ShotaroMaruyama</div>
        </div>
    </body>
</html>