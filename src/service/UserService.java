package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserBranchDepartment;
import dao.UserBranchDepartmentDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {
	/**
	 * ユーザー登録を提供
	 * @param user
	 */
	 public void insert(User user) {
	  Connection connection = null;
	  try {
		   // パスワード暗号化
		   String encPassword = CipherUtil.encrypt(user.getPassword());
		   user.setPassword(encPassword);

		   connection = getConnection();
		   new UserDao().insert(connection, user);

		   commit(connection);
	  } catch (RuntimeException e) {
		   rollback(connection);
		   throw e;
	  } catch (Error e) {
		  rollback(connection);
		  throw e;
	  } finally {
		  close(connection);
	  }
	 }

	 /**
	  * ログイン用照合
	  * @param account
	  * @param password
	  * @return
	  */
	 public User select(String account, String password) {
	  Connection connection = null;
	  try {
	   // パスワード暗号化
	   String encPassword = CipherUtil.encrypt(password);

	   connection = getConnection();
	   User user = new UserDao().select(connection, account, encPassword);
	   commit(connection);

	   return user;
	  } catch (RuntimeException e) {
	   rollback(connection);
	   throw e;
	  } catch (Error e) {
	   rollback(connection);
	   throw e;
	  } finally {
	   close(connection);
	  }
	 }

	 /**
	  * ユーザー一覧を提供
	  * @return
	  */
	 public List<UserBranchDepartment> select() {
		  Connection connection = null;
		  try {
		   connection = getConnection();
		   List<UserBranchDepartment> allUser = new UserBranchDepartmentDao().select(connection);
		   commit(connection);

		   return allUser;
		  } catch (RuntimeException e) {
		   rollback(connection);
		   throw e;
		  } catch (Error e) {
		   rollback(connection);
		   throw e;
		  } finally {
		   close(connection);
		  }
	}
	 /**
	  * ユーザー情報をIDで取得
	  * @param user_Id
	  * @return
	  */
	 public UserBranchDepartment select(int user_Id) {
		  Connection connection = null;
		  try {
		   connection = getConnection();
		   UserBranchDepartment user = new UserBranchDepartmentDao().select(connection,user_Id);
		   commit(connection);

		   return user;
		  } catch (RuntimeException e) {
		   rollback(connection);
		   throw e;
		  } catch (Error e) {
		   rollback(connection);
		   throw e;
		  } finally {
		   close(connection);
		  }
	 }
	 /**
	  * ユーザー情報更新
	  * @param user
	  */
	 public void update(User user) {
	 Connection connection = null;

		 try {
			 if(! StringUtils.isEmpty(user.getPasswordCheck())) {
				 // パスワード暗号化
				 String encPassword = CipherUtil.encrypt(user.getPassword());
				 user.setPassword(encPassword);
			 }
			connection = getConnection();
			new UserDao().update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {

			rollback(connection);
			throw e;}
		 catch (Error e) {

			 rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	 /**
	  * 凍結or凍結解除
	  * @param user
	  * @param isStopNum
	  */
	 public void switchStatus(int user, int isStopNum) {
	 Connection connection = null;
		 try {
			// パスワード暗号化
			connection = getConnection();
			new UserDao().switchStatus(connection, user, isStopNum);

			commit(connection);
		} catch (RuntimeException e) {

			rollback(connection);
			throw e;}
		 catch (Error e) {

			 rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}