package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserBranchDepartment;
import service.UserService;

@WebServlet("/management")
public class ManagementServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();

		//全ユーザー情報セット
		List<UserBranchDepartment> allUser = new UserService().select();
		User loginUser = (User) session.getAttribute("loginUser");
		request.setAttribute("loginUser", loginUser);
		request.setAttribute("allUser", allUser);
		request.getRequestDispatcher("management.jsp").forward(request, response);
	}
}
