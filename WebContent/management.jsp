<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/management.css">
<link rel="stylesheet" type="text/css" href="./css/default.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
    <body>

		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a>

		    <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <c:remove var = "errorMessages" scope = "session"></c:remove>

		<c:forEach items="${allUser}" var="user">
			<div class="user">
				<c:out value="アカウント:${user.account}" /><br />
				<c:out value="名前:${user.name}" /><br />
				<c:out value="支社:${user.branch_Name}" /><br />
				<c:out value="部署:${user.department_Name}" /><br />
				<form action="setting" method="get">
					<input type="hidden" name="user_id" value="${user.id}">
					<input type="submit" value="編集">
				</form>

				<script type="text/javascript">
				function check(){

					if(window.confirm('アカウント状態を変更してよろしいですか')){ // 確認ダイアログを表示

						return true; // 「OK」時は送信を実行

					}
					else{ // 「キャンセル」時の処理

						window.alert('キャンセルされました'); // 警告ダイアログを表示
						return false; // 送信を中止

					}

				}
				</script>

				<c:if test="${loginUser.id != user.id}">
					<c:if test="${user.is_Stopped == 0}">
						<span>アカウント状態:アクティブ</span>
							<form action="stop" method="post" onSubmit="return check()">
								<input type="hidden" name="is_stopped" value="1">
								<input type="hidden" name="user_id" value="${user.id}">
								<input type="submit" value="停止">
							</form>
					</c:if>
					<c:if test="${user.is_Stopped == 1}">
						<span>アカウント状態:停止</span>
							<form action="stop" method="post" onSubmit="return check()">
								<input type="hidden" name="is_stopped" value="0">
								<input type="hidden" name="user_id" value="${user.id}">
								<input type="submit" value="復活">
							</form>
					</c:if>
				</c:if>
			</div>
		</c:forEach>

</body>
</html>