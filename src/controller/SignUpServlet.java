package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import beans.UserBranchDepartment;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {

		List<Branch> allBranch = new BranchService().select();
		List<Department> allDepartment = new DepartmentService().select();

		request.setAttribute("allBranch", allBranch);
		request.setAttribute("allDepartment", allDepartment);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {
		List<String> errorMessages = new ArrayList<String>();
		User user = getUser(request);

		if (!isValid(user, errorMessages)) {
			List<Branch> allBranch = new BranchService().select();
			List<Department> allDepartment = new DepartmentService().select();
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("allBranch", allBranch);
			request.setAttribute("allDepartment", allDepartment);
			request.setAttribute("account", request.getParameter("account"));
			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request)
				throws IOException, ServletException {

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setPasswordCheck(request.getParameter("passwordCheck"));
		user.setName(request.getParameter("name"));
		user.setBranch_Id(Integer.valueOf(request.getParameter("branch_id")));
		user.setDepartment_Id(Integer.valueOf(request.getParameter("department_id")));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String passwordCheck = user.getPasswordCheck();
		int branch_Id = user.getBranch_Id();
		int department_Id = user.getDepartment_Id();
		List<UserBranchDepartment> allUser = new UserService().select();

		for(int i = 0; i < allUser.size(); i++) {
			if( account.equals(allUser.get(i).getAccount() )) {
				errorMessages.add("アカウント名が重複しています");
			}
		}

		if((StringUtils.isEmpty(account)) || account.matches("^[\\s|　]+$")) {
				errorMessages.add("アカウント名を入力してください");
		} else if(20 < account.length() || 6 > account.length()) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		}

		if(	(!(account.matches("^[a-zA-Z0-9]+$")))){
			errorMessages.add("アカウント名に使用できるのは半角英数字のみです");
		}

		if ((StringUtils.isEmpty(password)) || (StringUtils.isEmpty(passwordCheck)) ) {
				errorMessages.add("パスワードを入力してください");
		}

		if(((!StringUtils.isEmpty(password)) && (!StringUtils.isEmpty(passwordCheck))) ) {
			if(	(!(password.matches("^[a-zA-Z0-9!-/:-@\\[-`{-~]+$")))){
				errorMessages.add("パスワードに使用できるのは半角英数字と記号のみです");
			}

			if(20 < password.length() || 6 > password.length()) {
				errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
			}

			if(! passwordCheck.equals(password)) {
				errorMessages.add("確認用パスワードと一致しません");
			}
		}

		if(StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		} else {
			if(10 < name.length()){
				errorMessages.add("名前は10文字以下で入力してください");
			}
			if(	(name.matches("^(?=.*[\\s]).*$")) || (name.matches("^(?=.*[　]).*$"))){
				errorMessages.add("名前に空白は使用できません");
			}
		}


		if((branch_Id == 1) && (department_Id >= 3)) {
				errorMessages.add("支社と部署の組み合わせが正しくありません");
		}else if((branch_Id >= 2) && (department_Id <= 2)) {
			errorMessages.add("支社と部署の組み合わせが正しくありません");
		}

		if (errorMessages.size() != 0) {
				return false;
		}
		return true;
	}

}